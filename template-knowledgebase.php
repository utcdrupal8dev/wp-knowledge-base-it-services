<?php
/*
Template Name: Knowledge Base Page
*/
/**
 * Page Template for the Knowledge Base Page
 *
 * Publish a page and put this as a static front-page
 *
 * @package iPanelThemes Knowledgebase
 */

get_header();

$main_categories = get_categories( array(
    'taxonomy' => 'category',
    'parent' => 0,
    'hide_empty' => 0,
) );
?>
<div id="primary" class="content-area">
    <p class="introduction">This catalog provides information on the services offered and supported by the UTC Office of Information Technology.  The categories are based on the Educause taxonomy of IT service management for higher education institutions.</p>
    <main id="main" class="site-main" role="main">

        <div class="row kb-home-cat-row">
            <?php $cat_iterator = 0; foreach ( $main_categories as $cat ) : ?>
                <?php $term_meta = get_option( 'ipt_kb_category_meta_' . $cat->term_id, array() ); ?>
                <?php $term_link = esc_url( get_term_link( $cat ) ); ?>
                <?php $pcat_totals = ipt_kb_total_cat_post_count( $cat->term_id ); ?>
                
                <div class="col-md-4 card">
                    <h2 class="knowledgebase-title"><?php echo $cat->name; ?></h2>
                    <p><?php echo wpautop( $cat->description ); ?></p>

                    <div class="visible-xs">
                        <?php if ( isset( $term_meta['image_url'] ) && '' != $term_meta['image_url'] ) : ?>
                            <p class="text-center">
                                <a href="<?php echo $term_link; ?>">
                                    <img class="img-circle" src="<?php echo esc_attr( $term_meta['image_url'] ); ?>" alt="<?php echo esc_attr( $cat->name ); ?>" />
                                </a>
                            </p>
                        <?php endif; ?>
                        <div class="caption">
                            <?php if ( isset( $term_meta['support_forum'] ) && '' != $term_meta['support_forum'] ) : ?>
                                <p class="text-center"><a class="btn btn-default btn-block" href="<?php echo esc_url( $term_meta['support_forum'] ); ?>">
                                        <i class="glyphicon ipt-icon-support"></i> <?php _e( 'Get support', 'ipt_kb' ); ?>
                                    </a></p>
                            <?php endif; ?>
                        </div>
                    </div>

                    <div class="list-group">
                            <?php $cat_posts = new WP_Query( array(
                                'posts_per_page' => 4,
                                'cat' => $cat->term_id,
                                'orderby' => 'title',
                                'order' => 'ASC'
                            ) ); ?>
                            <?php if ( $cat_posts->have_posts() ) : ?>
                                <?php while ( $cat_posts->have_posts() ) : $cat_posts->the_post(); ?>
                                    <?php get_template_part( 'category-templates/content', 'popular' ); ?>
                                <?php endwhile; ?>
                            <?php else : ?>
                                <?php get_template_part( 'category-templates/no-result' ); ?>
                            <?php endif; ?>
                            
                            <?php wp_reset_query(); ?>
                    </div>

                    
                    <p class="text-center">
                        <a href="<?php echo $term_link; ?>" class="btn btn-default">
                            <?php printf( _n( 'Browse %d', 'Browse all %d', $pcat_totals, 'ipt_kb' ), $pcat_totals ); ?>
                        </a>
                    </p>
                </div>

                <?php $cat_iterator++; if ( $cat_iterator % 3 == 0 ) echo '<div class="clearfix"></div>'; ?>
            <?php endforeach; ?>
            <div class="clearfix"></div>
        </div>

        <?php wp_reset_query(); ?>
        <?php
        // Add the filter from the Posts Order By Plugin
        if ( function_exists( 'CPTOrderPosts' ) ) {
            add_filter( 'posts_orderby', 'CPTOrderPosts', 99, 2 );
        }
        ?>

        <?php // Finally add the actual page content ?>
        <?php if ( have_posts() ) : ?>
            <?php while ( have_posts() ) : the_post(); ?>
                <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                    <div class="entry-content">
                        <?php the_content(); ?>
                        <?php
                        wp_link_pages( array(
                            'before' => __( '<p class="pagination-p">Pages:</p>', 'ipt_kb' ) . '<ul class="pagination">',
                            'after'  => '</ul><div class="clearfix"></div>',
                        ) );
                        ?>
                    </div><!-- .entry-content -->
                </article>
            <?php endwhile; ?>
        <?php endif; ?>

    </main><!-- #main -->
</div><!-- #primary -->
<p>If there are corrections or additions to this page, please submit a ticket online at <a href="https://helpdesk.utc.edu/MRcgi/MRentrancePage.pl" title="UTC Help Desk">https://helpdesk.utc.edu</a>.
<?php get_footer(); ?>
