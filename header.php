<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package iPanelThemes Knowledgebase
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php wp_title( '|', true, 'right' ); ?></title>
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<!-- Add Brandbar -->
<script type="text/javascript" src="//www.utc.edu/_resources/brandbar/utc-brandbar-nojq.js"></script>	

<div id="page" class="hfeed site">



	<div id="content" class="site-content container">
<h2><?php bloginfo( $show = 'name' ); ?></h2>
	<?php ipt_kb_breadcrumb(); ?>
